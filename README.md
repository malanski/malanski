<div align="center">
<img height="420em" src="https://scontent.fbfh14-1.fna.fbcdn.net/v/t1.6435-9/p180x540/244771418_4380026338701045_1016906861568622985_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=e3f864&_nc_eui2=AeFhR1pV2e6QYt2ocJC1UNbyE8Jlz9eectQTwmXP155y1PtwuX6WNiQdMwyRG_5ucWtfAusNrkkz-VUuHeYhsPZG&_nc_ohc=2g7Nfe4SDCQAX-VTO88&_nc_ht=scontent.fbfh14-1.fna&oh=72bf1a4efde9d6ddd229a3c2fa1699fc&oe=61A2509C">
<img height="180em" src="https://github-readme-stats.vercel.app/api?username=malanski&show_icons=true&theme=dark&include_all_commits=true&count_private=true">  
</div>
<h1 align="center"><b>Hi, I'm Ulisses Malanski! welcome people! <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"></b></h1>

- I am graduated in Drawing and Art education. 
 
- Sou graduado em arte/desenho, tenho interesse nas linguagens e outras tecnologias, DAW e produção musical! 🔥
- 👀 I’m interested in technologies, visual art and music.  
- 🌱 I’m dedicated to Web Development and Web Full Stack!
- 💞️ I’m looking to collaborate on creativity with everyone. 
- This is my [youtube channel](https://www.youtube.com/channel/UCMO8be295Zay2OajfewJpMA) 
- HTML, CSS, PHP, React, Bootstrap, NodeJs, Javascript! 🔥
- I also can collaborate with: Design, Digital Art, Photo Manipulation, Concept Art, Audio Production, Music Composing.
- SOFTWARES.
  * Visual Art & Design.
      - Adobe Illustrator
      - Photoshop
      - Adobe XD
      - Adobe Premiere
  * Audio Editing & Music Production.
      - Ableton Live
      - Nuendo
      - Fruit Loops
        

<!---
TypeScript, C / C ++ / C #, .NET, Ruby, Angular, Java, Phyton
--->
##
<br>

<div align="center">
    <a href="https://www.facebook.com/ulisses.malanski/">
      <img height="180" style="border-radius: 50px;" src="https://lastfm.freetls.fastly.net/i/u/770x0/2d81602ce3cb43378ddf0d57407d9738.jpg#2d81602ce3cb43378ddf0d57407d9738">
     <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=malanski&layout=compact&langs_count=7&theme=radical">
</div>
  <div align="center" style="display: inline-block;"> <br>
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
<!---
<img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
<img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ruby/ruby-original.svg">
<img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/dot-net/dot-net-original.svg">
--->
<img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg">
<img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-plain.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/angularjs/angularjs-original.svg">
      <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-original.svg">
  </div>
  
  ##
 <br>
 
  <div align="center">
      <a href="www.linkedin.com/in/ulisses-malanski/" target="_blank"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
      <a href="https://www.instagram.com/ulissesmalanski_tattoo/" target="_blank"><img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
   
   
   </div>
 <br>
 <div align="center">
  
![nazaré da novela-gif](https://github.com/malanski/zero-bootstrap/blob/main/tenor.gif)
  </div>
